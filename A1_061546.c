#include <stdio.h>
#include <math.h>

void calculate(int a, int b, int c) {
	int disc = (b * b) - (4 * a * c);
	double RootOne = (-b - sqrt(disc))/ (2*a);
	double RootTwo = (-b + sqrt(disc))/ (2*a);

	if (disc < 0){
		printf("Complex.\n");
	}
	else if  (disc == 0) {
		printf("\nThe roots are real and equal.\n");
		printf("The root is:\n X = %.4f\n",RootOne);
	}
	else {
		printf("\nThe root are real and distinct.\n");
		printf("The roots are:\n X1 = %.4f\n X2 = %.4f\n",RootOne, RootTwo);
	}
 }

int main(void) {
	int a,b,c;

	printf("'a' cannot be zero.\n");
	printf("Enter the coefficient (a): ");
	scanf("%d",&a);
	printf("Enter the coefficient (b): ");
	scanf("%d",&b);
	printf("Enter the coefficient (c): ");
	scanf("%d",&c);

	if (a == 0){
		printf("Invalid input.\n");
		return 1;
	}

	calculate(a,b,c);
	return 0;
}